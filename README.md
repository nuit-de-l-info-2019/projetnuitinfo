Ceci est le projet de la Nuit de l'Info 2019 réalisé par l'équipe "Les mal barrés" de Lyon. Elle est constituée par :
* Pierre Hubert
* Mathis Chapuis
* Julia Rovjoska
* Marin Ruelen
* Noah Robereau
* Tanguy Frouin


Pour lancer la doc de Codeigniter: `make run_doc`

Pour lancer le serveur de Codeigniter: `make run_server`

Accès au serveur : http://51.83.255.172/

Accès à la forge : https://forge.univ-lyon1.fr/nuit-de-l-info-2019/projetnuitinfo

Accès au Docker : https://hub.docker.com/r/pierre42100/nuitinfo2019

## Construction et utilisation de l'image Docker
Le dockerfile se trouve dans le dossier `container_prod`. Pour construire le container, il faut se placer dans ce dossier et exécuter `./build_container.sh`.

Pour le lancer :

* Depuis le dépot Git : dans le même dossier `container_prod`, exécuter `./run_container.sh`
* Depuis le HubDocker : exécuter la commande `docker run -p 80:80 pierre42100/nuitinfo2019`. 

Vous pouvez alors accéder à la page `http://127.0.0.1` de votre navigateur pour tester le site. Le compte login : test / mot de passe : test vous permet de tester le site sans avoir à créer de compte.
