BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "questions" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"keyword"	TEXT,
	"message"	TEXT
);
INSERT INTO "questions" ("id","keyword","message") VALUES (1,'Fatigue','Pour lutter contre la fatigue intellectuelle, vous pouvez consulter ces sites https://www.aide.ulaval.ca/psychologie/textes-et-outils/difficultes-frequentes/l-epuisement-etudiant/ 
https://trendy.letudiant.fr/hyper-fatigue-j-arrive-pas-a-recuperer-a2653.html'),
 (295,'Burn-out','Comment déceler les signes du burn-out et lutter contre le burn-out:
https://trendy.letudiant.fr/burn-out-reagissez-avant-d-imploser-a2171.html
https://trendy.letudiant.fr/burn-out-etudiant-les-signes-qui-doivent-alerter-a4127.html'),
 (296,'Charge de travail','Si votre charge de travail est élevé, faites des pauses, reposez-vous afin d''éviter le burn-out. Pour plus d''informations sur le burn-out, visitez:
https://trendy.letudiant.fr/burn-out-etudiant-les-signes-qui-doivent-alerter-a4127.html'),
 (297,'Etude','Si vous n''arrivez pas à suivre vos études, dirigez-vous vers vos professeurs ou vos proches. Faites-leur part de vos difficultés, ils sauront vers qui vous orienter.'),
 (298,'Sommeil','Le sommeil est indispensable à l''apprentissage, il ne faut surtout pas le négliger. Fixez-vous strictement des heures et respectez les. Il faut au minimum 8h de sommeil pour un étudiant.'),
 (299,'Dépression','Si vous souffrez de dépression ou que vous en ressentez les signes, consultez ce site pour avoir la liste des numéros d''appels utiles : 
https://www.la-depression.org/numeros-dappels-utiles/'),
 (300,'Déprime','Si vous souffrez de dépression ou que vous en ressentez les signes, consultez ce site pour avoir la liste des numéros d''appels utiles : 
https://www.la-depression.org/numeros-dappels-utiles/'),
 (301,'Tension','Les études universitaires peuvent être sources de tensions.'),
 (302,'Dépressif','Si vous ressentez des signes de dépression comme ceux présentés sur le site https://www.studentjob.fr/blog/1734-9-signes-pour-reconnaitre-les-etudiants-qui-souffrent-d-une-depression
dirigez-vous vers les numéros d''appel utiles https://www.la-depression.org/numeros-dappels-utiles/'),
 (303,'Nervosité','Si vous êtes nerveux, faites une pause dans votre travail et reposez-vous. La lourde charge de travail demandée dans les études supérieures peut causer de la nervosité et du stress, voire même un burn-out.'),
 (304,'Nerveux','Si vous êtes nerveux, faites une pause dans votre travail et reposez-vous. La lourde charge de travail demandée dans les études supérieures peut causer de la nervosité et du stress, voire même un burn-out.'),
 (305,'Débordé','Etre débordé est l''un des signes de burn-out. La charge de travail étant de plus en plus grande dans les études supérieures, le burn-out peut arriver très rapidement.
Pour plus d''informations, consultez https://trendy.letudiant.fr/burn-out-reagissez-avant-d-imploser-a2171.html
 https://trendy.letudiant.fr/burn-out-etudiant-les-signes-qui-doivent-alerter-a4127.html
 Parlez-en aussi à vos professeurs de vos difficultés à suivre la charge de travail. Ils vous comprendront.'),
 (306,'Solitude','Si vous vous retrouvez seul, il y a des solutions pour sortir de la solitude.
 Vous pouvez vous rapprocher des structures médico-sociales liées à votre établissement universitaire. Cette dernière permet généralement de pouvoir rencontrer un psychologue et d’envisager avec lui (ou elle) les difficultés et souffrances qui se jouent.'),
 (307,'Stress','Le stress causé principalement par les études peut dégrader votre santé. Le stress peut vous amener à un burn-out et au décrochage scolaire. Pour prévenir de la nervosité et du potentiel burn-out engendrés par le stress, reposez-vous et faites une activité physique régulière. Cette dernière permet l''évacuation du stress et de se vider la tête après les cours.'),
 (308,'Stressé','Le stress causé principalement par les études peut dégrader votre santé. Le stress peut vous amener à un burn-out et au décrochage scolaire. Pour prévenir de la nervosité et du potentiel burn-out engendrés par le stress, reposez-vous et faites une activité physique régulière. Cette dernière permet l''évacuation du stress et de se vider la tête après les cours.'),
 (309,'Rémunération','Si vous manquez d''argent, vous vous demandez si vous devez travailler probablement pour gagner de l''argent afin de payer votre loyer et vos factures. L''association des études et des boulots étudiants peuvent vous fatiguer et entrainer une perte de concentration durant vos cours dû au manque de sommeil. Demandez à votre employeur si il est possible de modifer vos horaires de travail.'),
 (310,'Activité professionnelle','Si vous travaillez pour gagner de l''argent afin de payer votre loyer et vos factures, sachez qu''il faut faire attention avec le nombre d''heures de travail de votre activité professionnelle. L''association des études et des boulots étudiants peuvent vous fatiguer et entrainer une perte de concentration durant vos cours dû au manque de sommeil. Demandez à votre employeur si il est possible de modifer vos horaires de travail.
Si vous cherchez un job étudiant, dirigez-vous vers ce site vous proposant des jobs étudiants: https://jobetudiant.net/'),
 (311,'Métier','Si vous travaillez pour gagner de l''argent afin de payer votre loyer et vos factures, sachez qu''il faut faire attention avec le nombre d''heures de travail de votre activité professionnelle. L''association des études et des boulots étudiants peuvent vous fatiguer et entrainer une perte de concentration durant vos cours dû au manque de sommeil. Demandez à votre employeur si il est possible de modifer vos horaires de travail.
Si vous cherchez un job étudiant, dirigez-vous vers ce site vous proposant des jobs étudiants: https://jobetudiant.net/'),
 (312,'Travail','Si vous travaillez pour gagner de l''argent afin de payer votre loyer et vos factures, sachez qu''il faut faire attention avec le nombre d''heures de travail de votre activité professionnelle. L''association des études et des boulots étudiants peuvent vous fatiguer et entrainer une perte de concentration durant vos cours dû au manque de sommeil. Demandez à votre employeur si il est possible de modifer vos horaires de travail.
Si vous cherchez un job étudiant, dirigez-vous vers ce site vous proposant des jobs étudiants: https://jobetudiant.net/'),
 (313,'Malaise','Si vous ressentez un sentiment général de malaise, il est important de connaître les premiers signes. Ils sont listés ici : http://www.apsytude.com/fr/mes-soucis/moi-et-mon-mal-etre/mal-etre-depression-et-suicide/le-mal-etre/'),
 (314,'Mal-être','Si vous ressentez un sentiment général de malaise, il est important de connaître les premiers signes. Ils sont listés ici : http://www.apsytude.com/fr/mes-soucis/moi-et-mon-mal-etre/mal-etre-depression-et-suicide/le-mal-etre/'),
 (315,'Temps','Si vous n''arrivez pas à gérer votre temps entre études, job et loisirs, faites-vous un emploi du temps. Ce cadre vous permettra d''être plus organisé et d''économiser votre temps et de l''optimiser à votre avantage.'),
 (316,'Emploi','Si vous travaillez pour gagner de l''argent afin de payer votre loyer et vos factures, sachez qu''il faut faire attention avec le nombre d''heures de travail de votre activité professionnelle. L''association des études et des boulots étudiants peuvent vous fatiguer et entrainer une perte de concentration durant vos cours dû au manque de sommeil. Demandez à votre employeur si il est possible de modifer vos horaires de travail.
Si vous cherchez un job étudiant, dirigez-vous vers ce site vous proposant des jobs étudiants: https://jobetudiant.net/'),
 (317,'Courses','Si l''argent ne permet pas de manger à votre faim, vous pouvez vous tourner vers l''AGORAE, une épicerie solidaire: https://www.facebook.com/Agorae.Lyon'),
 (318,'Budget','Si votre budget est restreint, vous pouvez demander des aides financières comme la bourse: http://www.crous-lyon.fr/bourses/'),
 (319,'Soins','Si vous avez des problèmes de santé, tournez vous vers le service santé universitaire.'),
 (320,'Suicide','Si vous avez des pensées suicidaires, des personnes sont là pour vous aider: http://www.apsytude.com/fr/mes-soucis/moi-et-mon-mal-etre/mal-etre-depression-et-suicide/le-mal-etre/
https://solidarites-sante.gouv.fr/prevention-en-sante/sante-mentale-et-psychiatrie/article/que-faire-et-a-qui-s-adresser-face-a-une-crise-suicidaire'),
 (321,'Soucis',''),
 (322,'Eloignement familial','Nombre d’étudiants se retrouvent dans un espace de vie souvent restreint (studio ou chambre universitaire le plus souvent)  avec une problématique familiale qu’ils viennent de quitter et qui peut résonner plus moins fortement en chacun. Cet éloignement peut vous amener à l''isolement. Vous pouvez toujours continuer à appeler vos parents et vous faire des amis et les retrouver dans  un espace commun.'),
 (323,'Redoublement','Le fait de redoubler peut vous OUI'),
 (324,'Addiction','Si vous avez besoin d''aide à propos d''une addiction'),
 (325,'Handicap','Si vous avez une handicap, vous pouvez demander des aides:
https://www.aide-sociale.fr/aides-sante/aides-handicap/
https://www.service-public.fr/particuliers/vosdroits/N12230'),
 (326,'Invalidité','Si vous avez une handicap, vous pouvez demander des aides:
https://www.aide-sociale.fr/aides-sante/aides-handicap/
https://www.service-public.fr/particuliers/vosdroits/N12230'),
 (327,'Invalide','Si vous avez une handicap, vous pouvez demander des aides:
https://www.aide-sociale.fr/aides-sante/aides-handicap/
https://www.service-public.fr/particuliers/vosdroits/N12230'),
 (328,'Abonnement','Pour les abonnements TCL, consultez https://www.tcl.fr/tickets-abonnements/titres-et-tarifs'),
 (329,'Transport','Si vous avez des difficultés à vous déplacer, Vous pouvez vous tourner vers la carte jeune de la SNCF, les  formules d''abonnement 18-25 ans de TCL: 
https://www.sncf.com/fr/offres-voyageurs/cartes-tarifs-grandes-lignes/carte-jeune
https://www.tcl.fr/tickets-abonnements/titres-et-tarifs'),
 (330,'Tram','Si vous avez des difficultés à vous déplacer, Vous pouvez vous tourner vers la carte jeune de la SNCF, les  formules d''abonnement 18-25 ans de TCL:
https://www.tcl.fr/tickets-abonnements/titres-et-tarifs'),
 (331,'Métro','Si vous avez des difficultés à vous déplacer, Vous pouvez vous tourner vers la carte jeune de la SNCF, les  formules d''abonnement 18-25 ans de TCL:
https://www.tcl.fr/tickets-abonnements/titres-et-tarifs'),
 (332,'Surmenage',''),
 (333,'Sociabilité',''),
 (334,'Bus','Si vous avez des difficultés à vous déplacer, Vous pouvez vous tourner vers la carte jeune de la SNCF, les  formules d''abonnement 18-25 ans de TCL:
https://www.tcl.fr/tickets-abonnements/titres-et-tarifs'),
 (335,'Amis',''),
 (336,'Insertion',''),
 (337,'Etranger','Etre un étudiant étranger peut vous isoler des autres étudiants. Les barrières culturelles comme le langage peuvent vous donner des difficultés à vous adapter.'),
 (338,'Souffrances','Si vous souffrez, vous pouvez vous tourner vers des psychologues.'),
 (339,'Conditions de vie',''),
 (340,'Puces de lit','Si vous êtes dans une résidence CROUS, Contactez: https://www.tcl.fr/tickets-abonnements/titres-et-tarifs'),
 (341,'Adaptation',''),
 (342,'Epuisement',''),
 (343,'Malbouffe','Si vous avez un problème avec la malbouffe, vous pouvez consulter des nutritionistes et demander conseil au service santé de votre établissement.'),
 (344,'Secours','Pour les numéros d''urgences, consultez ce site: https://www.gouvernement.fr/risques/connaitre-les-numeros-d-urgence'),
 (345,'Colocation','Si le loyer est trop élevé , vous pouvez opter pour une colocation.');
COMMIT;
