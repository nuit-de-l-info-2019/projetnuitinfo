# Cahier des charges

## Ce qu'on va faire

### Chat bot

> Le site s'ouvre dessus. Le chat bot se base sur la base de donnée pour proposer des redirections à l'utilisateur

### Base de donnée de mot clés

> Fichiers textes pour les mots clés, catégorisés

### Mise en place de compte tuteurs (_ex : prof, bibliothécaire..._)

> Ils pourront seulement chatter avec les utilisateurs lambda (genre étudiant précaire)



## Technos à utiliser

* PHP pour le serveur
* Client en JS ?

## Liste des défis

* Dockerisation
* Jeu 404
* Félindra, tête de ******

## Répartition des tâches

* Chatbot
  * Julia
  * Mathis
* Connexion / Création de comptes
  * Marin
* Chat live
* Modèle des données (base SQLite - sqlitebrowser)
  * Noah
  * Tanguy
* Nulle part
  * Pierre