/**
 * Experts view script
 */
const pendingElem = document.getElementById("pendingConnections");
const conversation = document.getElementById("conversation");
const newMessageForm = document.getElementById("newMessageForm");

/** @type {HTMLInputElement} */
const messageInput = document.getElementById("messageInput");

/** @type {HTMLDivElement} */
const messagesTarget = document.getElementById("messages");

/** @type {HTMLButtonElement} */
const closeButton = document.getElementById("closeBtn");

let sessionID = -1;
let lastMessageID = 0;
let refreshInterval;

// Disconnect from all sessions
fetch("/api/disconnect_from_all_conversations");

async function refreshPendingList() {
    const result = await fetch("/api/pending_chats");
    const list = JSON.parse(await result.text());
    pendingElem.innerHTML = "";

    list.forEach(e => {
        const el = document.createElement("li");
        const wait_time = ((new Date()).getTime()/1000) - e.time_add;

        el.innerHTML = "Un étudiant attend depuis "+Math.floor(wait_time/60)+" minutes. <b>Se connecter</b>";
        pendingElem.appendChild(el);


        el.addEventListener("click", async () => {
            await acceptConversation (e.session_id);
        })
    })
}

async function acceptConversation (idAcceptedSession)
{
    const result = await fetch ("/api/acceptConversation?sessionId="+idAcceptedSession);

    conversation.style.visibility = "visible";

    sessionID = idAcceptedSession;

    refreshInterval = setInterval(() => {
        refreshMessages();
    }, 1000);
}

setInterval(() => refreshPendingList(), 1000);

newMessageForm.addEventListener("submit", async (e) => {
    e.preventDefault();

    const newMessage = messageInput.value;

    if(newMessage.length == 0)
        return;

    // Send message
    await fetch("/api/send_expert_message?sessionID="+sessionID+"&message="+encodeURIComponent(newMessage));

    messageInput.value = "";
});


async function refreshMessages() {
    const res = await fetch("/api/get_last_messages?last_message_id="+lastMessageID+"&sessionID="+sessionID);

    if(res.status == 404) {
        alert("La conversation a été fermée.");
        closeConversation();
        return;
    }

    const messages = JSON.parse(await res.text());

    messages.forEach(m => {
        lastMessageID = m.id;

        addMessage(m);
    });
}

function addMessage(m) {
    const newLi = document.createElement("li");
    newLi.innerHTML = "<b>"+(m.userID == -1 ? "Invité" : "Vous")+" :</b> " + m.message;
    messagesTarget.appendChild(newLi);
}

closeButton.addEventListener("click", async () => {

    //if(!confirm("Voulez-vous vraiment fermer cette conversation ?"))
        //return;

    await fetch("/api/close_conversation?sess_id=" + sessionID);

    closeConversation();
});

function closeConversation() {
    clearInterval(refreshInterval);
    conversation.style.visibility = "hidden";
    lastMessageID = 0;
    sessionID = -1;
}