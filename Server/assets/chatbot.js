const sessionID = Math.floor((Math.random() * 1000000) + 100);

let lastMessageId = 0;
let connected = false;

// Get HTML elements
/** @type {HTMLInputElement} */
const input = document.getElementById("message_input");
const send = document.getElementById("send_message");
const main_title = document.getElementById("main_title");
const sendButtonParent = document.getElementById("send_message").parentNode;

/** @type {HTMLDivElement} */
const messageArea = document.getElementById("messagesArea");

async function sendMessage() {
    if(input.value.length == 0)
        return;
    
    const message = input.value;
    
    if (!connected)
    {
        main_title.innerHTML = "Bonjour, je suis un chatbot, prêt à vous servir !";
        addMessageUI(message, true);
        const response = await fetch("/api/send_message?sess_id="+sessionID+"&message=" + encodeURIComponent(message));
        addMessageUI(await response.text(), false);
    }
    else {
        const response = await fetch("/api/send_client_message?sess_id="+sessionID+"&message="+encodeURIComponent(message));
    }
    

    input.value = "";
}


input.addEventListener("keyup", e => {
    if(e.key != "Enter")
    {
        return;
    }
    
    sendMessage();
})

function addMessageUI (message, isLocal)
{
    const el = document.createElement("li");
    el.className = "msg msg-" + (isLocal ? "sent" : "received");
    el.innerHTML = message;
    messageArea.appendChild(el);

    el.scrollIntoView(false);

    messageArea.innerHTML += "<li><br /></li>";
}

function close_conversation ()
{
    lastMessageId == -2;
    connected = false;
    addMessageUI("Vous avez été déconnecté du chat.", false);
    document.querySelector(".send_message").innerHTML = "D&eacute;connect&eacute;";
    main_title.innerHTML = "Connexion termin&eacute;e.";

}

async function refreshMessages ()
{
    let last_messages = await fetch ("/api/get_last_messages?last_message_id="+lastMessageId+"&sessionID="+sessionID);
    if (last_messages.status == 404)
    {
        close_conversation ();
        return;
    }
    last_messages = JSON.parse(await last_messages.text());
    

    last_messages.forEach(message => {
        addMessageUI(message.message, message.userID<=0);

        lastMessageId = message.id;
    });
}

/**
 * Join the conversation
 */
send.addEventListener("click", async e => {
    await fetch("api/request_live_chat?sess_id="+sessionID);

    const newElem = document.createElement("div");
    newElem.innerHTML = "En attente...";send_message
    newElem.className = "send_message";
    send.parentNode.appendChild(newElem);
    send.remove();

    // Make the request keeps alive
    let keepAliveInterval = setInterval(async () => {
        const response = await fetch("/api/keep_alive_chat?sess_id="+sessionID);

        if(response.status == 404)
            clearInterval(keepAliveInterval);
    }, 5000);

    let interval = setInterval(async () => {
        const response = await (await fetch("/api/get_info_user_for_chat?sess_id=" + sessionID)).text();
    
        if(response.length <= 1)
            return;
        
        clearInterval(interval);

        addMessageUI("Vous êtes désormais connecté avec "+response, false);
        main_title.innerHTML ="Vous êtes en communication avec "+response;

        document.querySelector(".send_message").innerHTML = "Se d&eacute;connecter";
        document.querySelector(".send_message").addEventListener("click", () => {
            fetch("/api/close_conversation?sess_id=" + sessionID);
        })

        connected = true;

        let refresh_interval = setInterval(() => {
            if (!connected)
            {
                clearInterval(refresh_interval);
                return;
            }
            refreshMessages();

        }, 1000)

    }, 2000);
})