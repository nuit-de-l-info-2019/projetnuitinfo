<?php

class Api extends CI_Controller {
    public function send_message ()
    {
        $message = $this->input->get("message");

        echo $response = $this->faq->search($message);
    }

    public function request_live_chat ()
    {
        $sessionId = $this->input->get("sess_id");
        $this->chat->createSessionRequest($sessionId);
    }

    public function keep_alive_chat() {
        $sessionId = $this->input->get("sess_id");
        
        if(!$this->chat->doesSessionExists($sessionId)) {
            http_response_code(404);
            echo "Conversation not found !";
            exit();
        }

        $this->chat->keepAliveSession($sessionId);
    }

    public function pending_chats() {
        echo json_encode($this->chat->getPendingList());
    }

    public function acceptConversation () {
        $sessionId = $this->input->get("sessionId");
        $this->chat->accept_conversation($sessionId, $_SESSION["user"]->id);
    }

    public function get_info_user_for_chat ()
    {
        $sessionId = $this->input->get("sess_id");
        $username = $this->chat->get_info_user_for_chat($sessionId);
        if ($username != -1)
        {
            echo $username;
        }
    }

    // Send a message from expert interface
    public function send_expert_message() {
        $this->chat->sendMessage($this->input->get("message"), 
                                $this->input->get("sessionID"),
                                $_SESSION["user"]->id);
    }

    public function send_client_message()
    {
        $this->chat->sendMessage($this->input->get("message"),
                                $this->input->get("sess_id"),
                                -1);
    }


    // Get the last messages of a conversatin
    public function get_last_messages(){
        $lastMessageID = $this->input->get("last_message_id");
        $sessionID = $this->input->get("sessionID");

        if(!$this->chat->doesSessionExists($sessionID)) {
            http_response_code(404);
            echo "Conversation not found !";
            exit();
        }


        echo json_encode($this->chat->lastMessagesSince($lastMessageID, $sessionID));
    }

    // Close a conversation
    public function close_conversation() {
        $sessionID = $this->input->get("sess_id");
        $this->chat->destroySession($sessionID);
    }

    // Close the conversations of the user
    public function disconnect_from_all_conversations() {
        $userID = $_SESSION["user"]->id;
        $this->chat->destroyUserChats((int)$userID);
    }
}