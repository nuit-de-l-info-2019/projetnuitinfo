<?php 

class Connexion extends CI_Controller{

	public function index(){

		// Sign out user if requested
		if($this->input->get("logout")) {
			$this->accounts->signOut();
			$success = "Au revoir !";
		}
		


		$login = $this->input->post("login");
		$password = $this->input->post("password");
		if ($login && $password)
		{
			$response = $this->accounts->createLogin($login, $password);
			$error = "Identifiant / Mot de passe incorrect !";
		}
		if (isset($response) && $response)
		{
			$_SESSION['user'] = $response;
			?><meta http-equiv="Refresh" content="0; url=/expertView" /><?php
			exit();
		}
		else
		{
			$this->load->view('connexion', array(
				"error" => isset($error) ? $error : false,
				"success" => isset($success) ? $success : false,
			));
		}
	}

	

}