<?php

class ExpertView extends CI_Controller {
    public function index() {
        if(!$this->accounts->signedIn()) {
            ?><meta http-equiv="Refresh" content="0; url=/connexion" /><?php
            exit();
        }

        $this->load->view('expert-view');
    }
}