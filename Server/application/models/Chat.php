<?php

class Chat extends CI_Model {

    function __construct(){
        parent::__construct();

        // Delete old entries
        $this->db->query("DELETE FROM chatList WHERE lastUpdate < ".(time() - 30));
    }

    function doesSessionExists(int $idSession) {
        $this->db->where("idSession", $idSession);
        $this->db->from("chatList");
        return $this->db->count_all_results() > 0;
    }

    function createSessionRequest($idSession)
    {
        if (!$this->doesSessionExists($idSession))
            $this->db->insert("chatList", array(
                "idSession" => $idSession,
                "timeInsert" => time(), 
                "idUser" => -1,
                "lastUpdate" => time()));
    }

    function keepAliveSession(int $sessionId) {
        $this->db->where("idSession", $sessionId);
        $this->db->set("lastUpdate", time());
        $this->db->update("chatList");
    }

    function getPendingList() : array {
        $this->db->where("idUser", "-1");
        $this->db->from("chatList");
        $query = $this->db->get();

        $results = array();
        foreach($query->result() as $row) {
            $results[] = array(
                "time_add" => $row->timeInsert,
                "session_id" => $row->idSession
            );
        }


        return $results;
    }

    function accept_conversation ($sessionId,$user_id)
    {
        $this->db->where("idSession", $sessionId);
        $this->db->set("idUser", $user_id);
        $this->db->update("chatList");
    }

    function get_info_user_for_chat ($sessionId)
    {
        $this->db->where("idSession", $sessionId);
        $this->db->from("chatList");
        $query = $this->db->get();

        foreach($query->result() as $result)
        {
            $username = $this->accounts->getNameById($result->idUser);
            return $username;
        }
        return -1;
    }

    /**
     * Send a message
     * 
     * Set user ID to 0 for messages from guest
     */
    public function sendMessage(string $message, int $sessionID, int $userID) {
        $this->db->insert("chatMessages", array(
            "sessionId" => $sessionID,
            "userId" => $userID,
            "message" => $message
        ));
    }

    /**
     * Get the last messages of the conversation
     */
    public function lastMessagesSince($lastMessageID, $sessionID) {
        $this->db->from("chatMessages");
        $this->db->where("id > ", $lastMessageID);
        $this->db->where("sessionID", $sessionID);
        $query =$this->db->get();

        $results = array();
        foreach($query->result() as $res) {
            $results[] = array(
                "id" => $res->id,
                "userID" => $res->userId,
                "message" => $res->message
            );
        }

        return $results;
    }

    /**
     * Destroy all session data
     */
    public function destroySession(int $sessionID) {
        $this->db->query("DELETE FROM chatList WHERE idSession = ".((int)$sessionID));
        $this->db->query("DELETE FROM chatMessages WHERE sessionId = ".((int)$sessionID));
    }

    /**
     * Destroy any chat connection related to a user
     */
    public function destroyUserChats(int $userID) {
        $this->db->query("DELETE FROM chatList WHERE idUser = " .  $userID);
    }
}