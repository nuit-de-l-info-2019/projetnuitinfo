<?php

class Faq extends CI_Model {
    function __construct(){
        parent::__construct();
        //load our second db and put in $db2
        $this->db2 = $this->load->database('faq2', TRUE);
    }

    function search ($message)
    {
        $response = "Excusez-moi, je comprends pas votre message.";

        $message = strtolower($message);
        $message_array = explode(" ", $message);
        foreach ($message_array as $word)
        {
            if (strlen ($word) > 4)
            {
                $query = $this->db2->query("SELECT * FROM questions WHERE keyword LIKE ?", 
                array("%".strtolower($word)."%"));
                foreach ($query->result_array() as $row)
                {
                    return $row['message'];
                }
            }
            
        }
        return $response;
    }
}