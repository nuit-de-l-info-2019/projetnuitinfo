<?php
/**
 * Account model
 */

class Accounts extends CI_Model {

    public function signedIn() : bool {
        return isset($_SESSION["user"]);
    }

    /**
     * Create a new account
     */
    public function create() {

    }

    public function createLogin ($login, $password)
    {
        $this->db->where("username", $login);
        $this->db->where("password", $password);
        $this->db->from("accounts");
        $query = $this->db->get();
        foreach($query->result() as $entry)
        {
            return $entry;
        }
        return false;
    }

    public function signOut() {
        if(isset($_SESSION["user"]))
            unset($_SESSION["user"]);
    }

    public function getNameById ($userId)
    {
        $this->db->where("id", $userId);
        $this->db->from("accounts");
        $query = $this->db->get();
        foreach($query->result() as $entry)
        {
            return $entry->username;
        }
        return null;
    }

}