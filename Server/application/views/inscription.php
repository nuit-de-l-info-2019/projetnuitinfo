<?php

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/assets/inscription.css">
	<title>Inscription</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
	<header>
		<!-- #BalanceTonHeader -->
	</header>
	<div id="inscription">
		<form method="post" action="/inscription" class="form_classique">
			<h1>Inscription</h1>

			<?php if($message) {
				?><div class="alert alert-success"><?= $message ?></div><?php
			} ?>

			<div class="form-group">
				Nom : 
				<input type="text" value="<?= isset($_POST['nom']) ? $_POST['nom'] : ''; ?>" name="nom" class="form-control">
			</div>

			<div class="form-group">
				Prénom : 
				<input type="text" value="<?= isset($_POST['prenom']) ? $_POST['prenom'] : ''; ?>" name="prenom" class="form-control">
			</div>

			<div class="form-group">
				Adresse mail : 
				<input type="text" value="<?= isset($_POST['mail']) ? $_POST['mail'] : ''; ?>" name="mail" class="form-control">
			</div>

			<div class="form-group">
				Identifiant : 
				<input type="text" value="<?= isset($_POST['id_inscri']) ? $_POST['id_inscri'] : ''; ?>" name="id_inscri" class="form-control">
			</div>

			<div class="form-group">
				Mot de passe : 
				<input type="password" value="<?= isset($_POST['mdp_inscri']) ? $_POST['mdp_inscri'] : ''; ?>" name="mdp_inscri" class="form-control">
			</div>

			<div class="form-group submit-btn">
				<button type="submit" class="btn btn-primary">S'inscrire</button>
			</div>
		</form>
	</div>
</body>
</html>