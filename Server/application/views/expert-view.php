<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//echo $nome;
?><!DOCTYPE html>
<html lang="en">
<head>
  <title>ChatHome - Interface d'aide</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/expert-view.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>
        <?= $_SESSION["user"]->firstname, " ", $_SESSION["user"]->name ?>
        <a href="/connexion?logout=true">D&eacute;connexion</a>
      </h4>
      <ul id="pendingConnections" class="nav nav-pills nav-stacked">
      </ul><br>
    </div>

    <div class="col-sm-9">
      <div id="conversation">
        <h1>Conversation <input id="closeBtn" value="Close" class="btn btn-danger" /></h1>
        <ul id="messages"></ul>
        <form class="newMessageForm" id="newMessageForm">
          <input type="text" id="messageInput" class="form-control" placeholder="Nouveau message..." />
          <input type="submit" class="btn btn-primary" value="Envoyer le message" />
        </form>
      </div>
		</form>
    </div>
  </div>
</div>

<footer class="container-fluid">
  <p>ChatHome</p>
</footer>

<script src="/assets/expertView.js"></script>

</body>
</html>
