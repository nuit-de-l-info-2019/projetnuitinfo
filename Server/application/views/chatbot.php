<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//echo $nome;
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>ChatBot</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="/assets/style.css">

</head>
<body>

<div class="jumbotron text-center">
	<button class="helpBtn btn btn-primary" type="button" onclick = "location.href='/connexion';">Pour ceux qui veulent aider</button>
	<h1 id="main_title">Bonjour, je suis un chatbot, prêt à vous servir !</h1>
</div>
<div id="container">
	<div class="chat_window">
	<div class="top_menu">
		<div class="title">Chat</div>
	</div>
	<ul class="messages" id="messagesArea">
		
	</ul>
	<div class="bottom_wrapper clearfix">
		<div class="message_input_wrapper">
			<input class="message_input" id="message_input" placeholder="Type your message here..." />
		</div>
		<div id="send_message" class="send_message">
			<div class="icon">
			</div>
			<div class="text">En parler avec un étudiant</div>
		</div>
	</div>
</div>

</div>

<script type="text/javascript" src="/assets/chatbot.js"></script>

</body>
</html>