<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//echo $nome;
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>ChatBot</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="/assets/connexion.css">

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>


<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <h2>Connexion</h2>

    <?php if($error) {  ?>
    <div class="alert alert-danger"><?= $error ?></div>
    <?php } ?>

    <?php if($success) {  ?>
    <div class="alert alert-success"><?= $success ?></div>
    <?php } ?>

    <!-- Login Form -->
    <form method="post" action="/connexion">
      <input type="text" id="login" name="login" class="fadeIn second" name="login" placeholder="login">
      <input type="password" id="password" name="password" class="fadeIn third" name="login" placeholder="password">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="/inscription">Rejoindre l'équipe</a><br />
      <a class="underlineHover" href="/">Annuler et revenir à l'écran précédent</a>
    </div>

  </div>
</div>

</body>
</html>